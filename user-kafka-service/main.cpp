#include <csignal>

#include <kafka/KafkaClient.h>
#include <kafka/KafkaProducer.h>
#include <kafka/KafkaConsumer.h>

#include <database/User.h>

using namespace kafka;
using namespace kafka::clients::consumer;
using namespace kafka::clients::producer;



void producer() {
    // E.g. KAFKA_BROKER_LIST: "192.168.0.1:9092,192.168.0.2:9092,192.168.0.3:9092"
    const std::string brokers = getenv("KAKFA_BOOTSTRAP_SERVERS");
    const Topic topic = getenv("KAFKA_TOPIC");

    // Prepare the configuration
    const Properties props({{"bootstrap.servers", brokers}});

    // Create a producer
    KafkaProducer producer(props);

    // Prepare a message
    std::cout << "Type message value and hit enter to produce message..." << std::endl;
    std::string line = "asfasfasfasf";
    // std::getline(std::cin, line);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    database::User user;
    user.first_name() = "admin";
    user.last_name() = "Tompson";
    user.email() = "1@mail.com";
    user.uuid() = "uasfs-fasf-v-svas-asf";
    user.password() = "admin";
    user.login() = "admin";

    std::stringstream ss;
    Poco::JSON::Stringifier s;
    s.stringify(user.toJSON(), ss);
    auto payload = ss.str();

    // ProducerRecord record(topic, NullKey, Value(line.c_str(), line.size()));
    ProducerRecord record(topic, NullKey, Value(payload.c_str(), payload.size()));

    // Prepare delivery callback
    auto deliveryCb = [](const RecordMetadata& metadata, const Error& error) {
        if (!error) {
            std::cout << "Message delivered: " << metadata.toString() << std::endl;
        }
        else {
            std::cerr << "Message failed to be delivered: " << error.message() << std::endl;
        }
    };

    // Send a message
    producer.send(record, deliveryCb);

    // Close the producer explicitly(or not, since RAII will take care of it)
    producer.close();
}

std::atomic_bool running = {true};
void stopRunning(int sig) {
    if (sig != SIGINT) return;

    if (running) {
        running = false;
    } else {
        // Restore the signal handler, -- to avoid stuck with this handler
        signal(SIGINT, SIG_IGN); // NOLINT
    }
}

int main() {
    // std::thread producer_thread(producer);

    // Use Ctrl-C to terminate the program
    signal(SIGINT, stopRunning);    // NOLINT

    const std::string brokers = getenv("KAKFA_BOOTSTRAP_SERVERS"); // NOLINT
    const Topic topic = getenv("KAFKA_TOPIC");            // NOLINT

    // Prepare the configuration
    const Properties props({{"bootstrap.servers", {brokers}}});

    database::User::init();

    // Create a consumer instance
    KafkaConsumer consumer(props);

    // Subscribe to topics
    consumer.subscribe({topic});

    std::cout << "Starting listening kafka " << topic << std::endl;
    while (running) {
        // Poll messages from Kafka brokers
        auto records = consumer.poll(std::chrono::milliseconds(100));

        for (const auto& record : records) {
            if (!record.error()) {
                std::cout << "Got a new event [" << record.offset() << "] " << record.value().toString() << std::endl;                
                auto user = database::User::fromJSON(record.value().toString());
                user.save_to_mysql();
            }
            else {
                std::cerr << record.toString() << std::endl;
            }
        }
    }

    // No explicit close is needed, RAII will take care of it
    consumer.close();

    // producer_thread.join();
}
