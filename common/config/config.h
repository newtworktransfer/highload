#ifndef CONFIG_H
#define CONFIG_H

#include <string>

class  Config{
    private:
        Config();
        std::string _host;
        std::string _port;
        std::string _login;
        std::string _password;
        std::string _database;

        std::string cache_host_;
        std::string cache_port_;
        bool use_cache_;


        std::string kafka_boostrap_servers_; 
        std::string kafka_topic_;

        size_t max_shards_number_;
    public:
        static Config& get();

        std::string& port();
        std::string& host();
        std::string& login();
        std::string& password();
        std::string& database();

        const std::string& get_port() const ;
        const std::string& get_host() const ;
        const std::string& get_login() const ;
        const std::string& get_password() const ;
        const std::string& get_database() const ;

        const std::string& get_cache_host() const { return cache_host_; }
        const std::string& get_cache_port() const { return cache_port_; }
        bool get_use_cache() const { return use_cache_; }

        const std::string& get_kafka_boostrap_servers() const { return kafka_boostrap_servers_; }
        const std::string& get_kafka_topic() const { return kafka_topic_; }
        size_t get_max_shards_number() const { return max_shards_number_; }
};

#endif