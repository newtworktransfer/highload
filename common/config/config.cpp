#include <iostream>
#include <stdexcept>
#include "config.h"

Config::Config() {

    auto GetEnv = [](const std::string& name, bool required = true) {
        char* ptr = std::getenv(name.c_str());
        if (ptr == nullptr) {
            if (required)
                throw std::runtime_error("error: '" + name + "' does not exported");
            std::cout << "[ATTENTION] '" + name + "' does not exported using default value: ''" << std::endl;
            return std::string();
        }
        return std::string(ptr);
    };

    _host = GetEnv("DB_HOST");
    _port = GetEnv("DB_PORT");
    _login = GetEnv("DB_LOGIN");
    _password = GetEnv("DB_PASSWORD");
    _database = GetEnv("DB_DATABASE");

    auto max_shards_number_str = GetEnv("DB_MAX_SHARDS_NUMBER", false);
    if (!max_shards_number_str.empty())
        max_shards_number_ = std::stoul(max_shards_number_str);
    else
        max_shards_number_ = 1;

    std::cout << "[INFO] MAX_SHARDS_NUMBER: " << max_shards_number_ << std::endl;

    use_cache_ = GetEnv("USE_CACHE", false).length();
    std::cout << "[INFO] USE_CACHE: " << (use_cache_ ? "true" : "false") << std::endl;

    kafka_boostrap_servers_ = GetEnv("KAKFA_BOOTSTRAP_SERVERS", false);
    kafka_topic_ = GetEnv("KAFKA_TOPIC", false);

    cache_host_ = GetEnv("CACHE_HOST", false);
    cache_port_ = GetEnv("CACHE_PORT", false);
}

Config &Config::get()
{
    static Config _instance;
    return _instance;
}

const std::string &Config::get_port() const
{
    return _port;
}

const std::string &Config::get_host() const
{
    return _host;
}

const std::string &Config::get_login() const
{
    return _login;
}

const std::string &Config::get_password() const
{
    return _password;
}
const std::string &Config::get_database() const
{
    return _database;
}

std::string &Config::port()
{
    return _port;
}

std::string &Config::host()
{
    return _host;
}

std::string &Config::login()
{
    return _login;
}

std::string &Config::password()
{
    return _password;
}

std::string &Config::database()
{
    return _database;
}