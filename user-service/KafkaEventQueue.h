#pragma once

#include <kafka/KafkaProducer.h>
#include <database/User.h>

class KafkaEventQueue {
private:
    std::unique_ptr<kafka::clients::producer::KafkaProducer> producer_;
    kafka::Topic topic_;

    KafkaEventQueue();
public:

    void Initialize();

    static KafkaEventQueue& Instance();

    void PushCreateUserEvent(const database::User& user);
};