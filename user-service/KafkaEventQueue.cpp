#include "KafkaEventQueue.h"
#include <kafka/KafkaProducer.h>

#include <database/User.h>
#include <config/config.h>


KafkaEventQueue::KafkaEventQueue() {
    Initialize();
}

void KafkaEventQueue::Initialize() {
    // Prepare the configuration
    const kafka::Properties props({{"bootstrap.servers", Config::get().get_kafka_boostrap_servers()}});

    // Create a producer
    producer_ = std::make_unique<kafka::clients::producer::KafkaProducer>(props);
    topic_ = Config::get().get_kafka_topic();
}

KafkaEventQueue& KafkaEventQueue::Instance() {
    static KafkaEventQueue eq;
    return eq;
}

void KafkaEventQueue::PushCreateUserEvent(const database::User& user) {
    std::stringstream ss;
    Poco::JSON::Stringifier s;
    s.stringify(user.toJSON(true), ss);
    auto payload = ss.str();

    auto payload_wrapped = std::make_shared<std::string>(std::move(payload));

    kafka::clients::producer::ProducerRecord record(topic_, kafka::NullKey, kafka::Value(payload_wrapped->c_str(), payload_wrapped->size()));

    // Awful moment, but ProducerRecord does not take ownership and copy payload...
    // Keep payload in lambda scope
    auto deliveryCb = [payload_wrapped](const kafka::clients::producer::RecordMetadata& metadata, const kafka::Error& error) {
        if (!error) {
            std::cout << "Message delivered: " << metadata.toString() << std::endl;
        }
        else {
            std::cerr << "Message failed to be delivered: " << error.message() << std::endl;
        }
    };

    // Send a message
    producer_->send(record, deliveryCb);
}
