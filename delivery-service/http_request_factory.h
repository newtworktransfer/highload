#pragma once
#include <iostream>
#include <optional>

#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/JSON/Object.h>

std::optional<Poco::JSON::Object::Ptr> AuthUser(const Poco::Net::HTTPServerRequest& request);

template <typename T>
class AuthRequired : public Poco::Net::HTTPRequestHandler {
private:
    T handler;

public:
    void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) override {
        try {
            response.set("Access-Control-Allow-Credentials", "true");
            response.set("Access-Control-Allow-Origin", "*");
            if (request.getMethod() == Poco::Net::HTTPRequest::HTTP_OPTIONS) {
                response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);

                if (request.has("Access-Control-Request-Headers")) {
                    auto required_headers = request.get("Access-Control-Request-Headers");
                    response.set("Access-Control-Allow-Headers", required_headers);
                }

                if (request.has("Access-Control-Request-Method")) {
                    auto required_methods = request.get("Access-Control-Request-Method");
                    response.set("Access-Control-Allow-Methods", required_methods);
                }

                response.send();
                return;
            }

            std::cout << "[INFO] Run authentication ..." << std::endl;
            auto user = AuthUser(request);
            if (!user.has_value())
                throw std::logic_error("wrong login/password");

            std::cout << "[INFO] Authentication success" << std::endl;
            return handler.handleRequest(request, response);
        }
        catch (const std::exception& e) {
            std::cout << "[WARNING] Auth exception: " << e.what() << std::endl;
        }
        catch (...) {
            std::cout << "[WARNING] Auth exception: unknown" << std::endl;
        }

        if (!response.sent()) {
            response.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED);
            response.send();
        }
    }
};

class HTTPRequestFactory: public Poco::Net::HTTPRequestHandlerFactory {
private:
    std::string format_;

public:
    HTTPRequestFactory() = default;

    Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request) override;
};
